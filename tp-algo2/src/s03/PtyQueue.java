package s03;

import java.util.Date;

public class PtyQueue<E, P extends Comparable<P>> {
  private final Heap<HeapElt> heap;
  // ------------------------------------------------------------
  public PtyQueue() {
    heap = new Heap<HeapElt>();
  }
  // ------------------------------------------------------------
  public boolean isEmpty() {
    return heap.isEmpty();
  }

  /** Adds an element elt with priority pty */ 
  public void enqueue(E elt, P pty) {
      heap.add(new HeapElt(pty, elt));
  }
  
  /** Returns the element with highest priority. PRE: !isEmpty() */ 
  public E consult() {
    assert !isEmpty(); //PRE
    return heap.min().getElt();
  }

    /** Returns the priority of the element with highest priority.
     *  PRE: !isEmpty() */
    public P consultPty() {
        return heap.min().getPty();
    }


    /** Removes and returns the element with highest priority.
   *  PRE: !isEmpty() */ 
  public E dequeue() {
    assert !isEmpty(); //PRE
    return heap.removeMin().elt;
  }

  @Override public String toString() {
    return heap.toString(); 
  }
  //=============================================================
  class HeapElt implements Comparable<HeapElt> {
    E elt;
    P pty;
    Date date;

    public HeapElt(P thePty, E theElt) {
      this.elt = theElt;
      this.pty = thePty;
      this.date = new Date();
    }

    @Override public int compareTo(HeapElt arg0) {
        return pty.compareTo(arg0.pty);
    }

      public E getElt() {
          return elt;
      }

      public P getPty() {
          return pty;
      }
  }
}

