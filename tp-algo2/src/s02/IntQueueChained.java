package s02;

import java.util.Arrays;

public class IntQueueChained {

  //Queue variables
  private final static int NIL = -1;
  private int front = NIL;
  private int back = NIL;


  //Pointer variables
  private static int[] elts = new int[10];
  private static int[] nexts = {1, 2, 3, 4, 5, 6, 7, 8, 9, NIL};
  private static int firstFreeCell = 0;

  public IntQueueChained() {
  }

  public void enqueue(int elt) {
    int aux = allocate();//get current free cell index
    if (back != NIL) nexts[back] = aux;//points to the next (new) used cell
    elts[aux] = elt; //Put the element in the current free cell
    back = aux; //Moves the back to the last element added
    if (front == NIL) front = aux; //If there's no front (empty list) then it begins at this cell
  }

  public boolean isEmpty() {
    return front == NIL;
  }

  public int consult() {
    return elts[front];
  }

  public int dequeue() {
    int e = elts[front]; //get the element at the front of the queue
    int newFront = nexts[front]; //prepare the new front where the cell at the front of the queue is pointing to
    deallocate(front);//"free" the memory of the cell that was at the front of the queue
    if (front == back) back = NIL;
    front = newFront;
    return e;
  }

  /**
   * The firstFreeCell is updated to the one the current free cell is pointing to.
   * The current free cell points to NIL (it now is the last one used)
   *
   * If we run out of memory, the arrays are doubled
   * @return the index of the first free cell to enqueue the element
   */
  private static int allocate() {
    if (firstFreeCell == NIL) { //In case we run out of memory
      //Needed to begin the next "free memories"
      int oldLength = elts.length;

      // Copy old arrays elements
      elts = Arrays.copyOf(elts, elts.length *2);
      nexts = Arrays.copyOf(nexts, nexts.length *2);

      // Prepare/link new free memory
      for (int i = oldLength; i < nexts.length - 1; i++) {
        nexts[i] = i + 1;
      }
      nexts[nexts.length - 1] = NIL;//Add last cell as NIL
      firstFreeCell = oldLength;//Buffer for next firstFreeCell

    }
    int i = firstFreeCell; //Current free cell to be returned
    firstFreeCell = nexts[i];//firstFreeCell gives the index of the new first cell to be free (that the current cell points to)
    nexts[i] = NIL;//The current cell is -1, last to be used
    return i;//return the index for the current cell to be filled with an element.
  }

  /**
   * The "memory space" being freed becomes the new first cell to be used, and points toward the now "old" firstFreeCell
   * @param i int i is the index of the cell at the front of the queue
   */
  private static void deallocate(int i) {
    nexts[i] = firstFreeCell;//Points to a free cell (known thanks to firstFreeCell)
    //no need to nexts[i] = 0 to "clear" the memory, it will be replaced when needed.
    //The old value still exists in the meantime but should not be accessible
    firstFreeCell = i;//This memory space becomes the new firstFreeCell to be used for the next enqueue.
  }
}
