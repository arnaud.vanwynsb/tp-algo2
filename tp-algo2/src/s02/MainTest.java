package s02;


public class MainTest {
    public static void main(String[] args){
    float[] t = {4,2,2,8,8,3,1,5};

    float biggest=t[0];
    int etoile = 0;
        for (int i = 0; i < t.length; i++) {
            if(t[i]>biggest){
                etoile++;
            }
        }
        System.out.println(etoile);
        //Je voudrais comparer le nombre de nombre differents et faire cela fois "etoile", mais je ne vois pas comment en O(n) de cette maniere

        int[] ar1 = new int[]{1};
        int[] ar2 = new int[]{1,0,1};
        System.out.println(isOk(ar1,0)); //true
        System.out.println(isOk(ar2,1)); //true
        int[] ar3 = new int[]{0,1};
        System.out.println(isOk(ar3,0));

        int[] ar4 = new int[]{2,0,2,-1,1};
        System.out.println(isOk(ar4,1));

    }

    //return true si ses voisins (gauche et droite si possible) sont plus petit ou egal a t[r]
    static boolean isOk(int[] t, int r){
        int min = t[0];
        for(int i=1;i<r+1;i++){ //r+1 as a length so that it doesnt find a minimum further away as in ar4
            if(t[i] <= min){
                min = t[i];
            }
        }
        return min==t[r];
    }


}
